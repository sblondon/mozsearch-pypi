# MozSearch PyPI

Add a search engine to search in Python Package Index hosted by pypi.org.

A shortcut is available if you use awesome bar: @pypi

Available for users at:
https://addons.mozilla.org/fr/firefox/addon/pypi-search/

Released under GPL licence v3 or upper
